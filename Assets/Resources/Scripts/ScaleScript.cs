﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleScript : MonoBehaviour {

	public float scaleSpeed = 20;
	public float scaleMin = 1;
	public float scaleMax = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = new Vector3 (transform.localScale.x, Mathf.PingPong (Time.time * (scaleSpeed / 10), scaleMax - scaleMin) + scaleMin, transform.localScale.z);
	}
}
