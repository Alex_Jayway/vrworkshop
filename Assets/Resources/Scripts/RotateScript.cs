﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour {

	public float rotationSpeed = 20;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update() {
		// Rotate the object around its local X axis at rotSpeed degree per second
		transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
	}

}
