﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour {

	private float movementSpeed = 20;
	public float xPositionMin = -5;
	public float xPositionMax = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update() {
		transform.position = new Vector3 (Mathf.PingPong (Time.time * (movementSpeed / 10), xPositionMax - xPositionMin) + xPositionMin, 1, 0);
	}

}
