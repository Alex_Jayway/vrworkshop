#VR workshop - Introduction to VR development with Unity
In this workshop you will learn the basics of how to set up a new project in Unity and to get started with the development of Virtual Reality applications. The session will begin with a short introduction to VR and Unity, together with two convenient plugins that can be used to more rapidly start building apps for VR. The workshop will then continue with practical assignments that everyone can work with at their own pace. Help, guidance and support will be around for any questions during the workshop. 

##Structure of the workshop:
The assignments are divided into steps where you follow an example project with the goal to replicate it. During the workshop you will work in a scene with the name **WorkingScene**. This scene is where you make all your changes while following the tutorial. When you feel like you have completed a step, or need some extra advice to solve it, you can either ask for help or have a look at the solution. The Unity project includes all the solutions to all steps. Each solution scene can be found in the project directory with the path: *Resources/Scenes/SolutionScenes[x]*. When your solution looks similar to the solution scene, remember to switch back to your **WorkingScene** and then continue to the next assignment.

![image missing](docs/img/1.png =0.5x)

##Step 1 - Explore the project
To get started, try to find the scene with the name **WorkingScene**. You can either search for it, or find it with the path: Resources/Scenes, in the Project window. Once selected, make sure that this scene is shown in the top of the hierarchy.

![image missing](docs/img/2.png)

In the image above you are also able to see all the game objects that the scene contains. The two objects with the name VRTK holds the basic settings for running the VR application, e.g. which SDK to use and event handlers for detecting button actions on the VR controllers. The default SDK that in this project is SteamVR, but there is also a fallback simulator SDK if no VR equipment is connected to your computer. To test the current scene with the simulator, press the play button located above the scene view window.

![image missing](docs/img/3.png)

You are now running the scene in Game mode, which make it possible for you to move and look around in the 3d-world. Toggle the controller hints for the simulator by pressing **F1 button** on your keyboard. To leave Game mode, press the play button again to enter Scene mode.

In the hierarchy window above, there are two additional game objects already added to the scene. Select Directional Light and look in the Inspector window to find out which components that are attached. All game objects in a scene automatically gets a Transform component once created or added where the position, rotation and scale is specified. This component cannot be removed since the game engine needs this information to place the object in the 3d-world.

![image missing](docs/img/4.png)

The second component attached is named Light. Normally, at least one game object in a scene acts as a light source and is responsible for controlling the light. Try to toggle the light by unchecking the box at the top left to disable the component. If you want, you can now enter Game mode to try out your scene in “night mode”. Toggle back your Light component and explore by playing around with the settings for Type and Color. Make sure you switch back the settings for type to *Directional* and *Color* of the light to hex color: *#FFF4D6FF*.

The last game object added to the scene from start is named Floor. Select it in the hierarchy window and check out attached components in the Inspector window. Note that the object has a Transform component, which as mentioned before is required. Try moving the object by setting its transform position y to **5** instead of **0**. Enter Game mode and find out what happened. Make sure you reset the position again to **x,y,z = 0,0,0**.

The next component in the Inspector reveals that the Floor object is made out of a mesh called Cube. This is one of the standard 3D objects in Unity. Examples of other standard 3D objects are Spheres, Cylinders and Planes. Inspect the cube mesh by selecting Cube in the component window. 

![image missing](docs/img/5.png)

You might wonder why your gameobject Floor does not look like the mesh it is represented by. Go back to your Floor component in the hierarchy and look at the Transform component. Here you can see that the scale is set to **x,y,z = 20,1,20**. This reveals that the object is scaled by factor 20 in both x and z direction. Try setting the scale to **x,y,z = 1,1,1** and see what happens. Make sure to reset the scale to **x,y,z = 20,1,20**.

The third component is a Box Collider. This component defines a cube area where collisions with other game objects are detected. On this Floor object the Box Collider will be used to prevent objects falling through.

The fourth component is a Mesh Renderer. In this component you can control lightning and what materials that will be rendered on your mesh. Open up Materials and you will see that a Default-Material is used which corresponds to the Material that is located right below the component. A mesh could have several materials attached but is required to have at least one. This Default-Material cannot be modified. While still having the Floor object selected, try finding a file named **FloorMaterial** in the Project window.

![image missing](docs/img/6.png)

To attach this material to our Floor object you can either drag it and drop it on the Floor object in the scene, or drop it below the Default-Material in the Inspector window to replace it. With the default material replaced, you are now able to modify it. Try changing the color of the material by selecting the small white window. In this project the picked color has the hex value: *#C79363FF*.

To finish of this first assignment you can now switch scene to the **SolutionScene1** to make sure that your **WorkingScene** looks the same. If it does, make sure to switch back to your **WorkingScene** and move on to the next step.

##Step 2 - Adding your first 3D-models

In this assignment you will start adding your own game objects to your scene. As mentioned in the previous step, Unity has a few standard 3D-models ready that can be added to your scene. Add a new gameobject by selecting GameObject in the top menu, 3D Object and then Cube.

![image missing](docs/img/7.png)

You can now see that a new gameobject named Cube has been added to your hierarchy. Explore by adding a few different game objects to your scene. Try to change their position, rotation and scale by modifying their Transform component.

![image missing](docs/img/8.png)

Here is an example of how your scene might look after you have been added a few gameobjects and modified their transform.

##Step 3 - Adding scripts to gameobjects
In order to make your scene feel more alive, you will now in this assignment add some scripts to your game objects. Like other components in Unity, Scripts can can be applied to a gameobject. A game object can have more than one script component.

Pick one of the game objects that you created in previous assignments, e.g. a cube, and look at the Inspector window where the attached components are listed. At the bottom there is a button named **Add Component**. Search for **MoveScript** and press enter. You should now see that the script has been applied to your game object.   

![image missing](docs/img/9.png)

This script will move the gameobject’s transform position x between the values -5 to 5 back and forth. Try run the scene to see if the object that you applied the script to is moving. Stop the game and explore by changing min and max values for x.

To find out more about the running script, click on MoveScript (see image above). This will open MonoDevelop, the integrated development environment (IDE) supplied with Unity. In the script you can see that variables xPositonMin and xPositionMax corresponds to the values that you changed before in Unity (see above image). However, the variable movementSpeed in the script was not visible. The reason for this is that only public variables in scripts are visible in the editor. Try making movementSpeed public, save the script and go back to Unity. The variable should now appear beside the other ones. Explore by changing the value of movement speed in Unity.

The MoveScript is written in the program language C#, which is the most common language used in Unity projects. When a new script is created it automatically inherits from the base class MonoBehaviour and adds the two functions; **Start()** and **Update()**. From Unity documentation the following can be read:

* “Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.”
* “Update is called every frame, if the MonoBehaviour is enabled.”

When entering Game mode, a script that is applied to a gameobject and inherits from MonoBehaviour will automatically run the Start function on the first frame to initialize variables and thereafter the Update function will be called every frame. 

Continue the assignment by exploring some other scripts that was prepared for this workshop. Begin with adding each of the scripts below to a unique gameobject so that it only has one applied.

* MoveScript
* RotateScript
* ScaleScript
* RGBScript

Try to understand what is happening in the code and test it out in Game mode. What happens if you try to apply more than one of the scripts above to a single gameobject?

##Step 4 - Interactable objects with VRTK
You have in this workshop so far been learning about what a game object is, how to create it and add it to your scene, and to manipulate their Transform component by different kinds of scripts. We will now continue by preparing your game objects for VR. This will make it possible to grab, lift and throw your created game objects in VR using the VR controllers.

From previous assignment you learned that a game object can be manipulated by scripts. In order to grab a game object using the VR controller and make it follow your hand there are quite many scripts that needs to be written to achieve this. The scripts needs to detect:

* When your controller touches the specific gameobject that you want to lift
* If you are holding down a certain button at the same time as you touch it to be able to lift it
* Translating your controller’s position and applying them to the game object’s transform
* Listen for button release so that you can drop the gameobject

This is only a few things that need to be handled by scripts and you probably realize that writing all of them are quite complex and time consuming. Luckily, in most of the cases you don’t have to. By using the open source library VRTK, all the necessary scripts for handling basic object interaction will be available in your project. By not having to write them yourself, you can more rapidly build VR applications in Unity and focus on the content instead. The library offers common solutions such as:

* Support for SteamVR
* Object interactions, e.g. touching, grabbing, using objects
* Highlighting objects on touch
* VR Simulator
* Laser pointers on controllers
* Teleporting
* Controller haptic feedback

It is now time to create your first interactable object. Start by removing all game objects that you created from previous assignment. It should only be the floor object left in the hierarchy together with Directional Light and VRTK.

![image missing](docs/img/10.png)

Create a new cube and position it at the center of the floor. Make sure your Cube is selected and then go to *Window -> VRTK -> Setup* interactable object.

![image missing](docs/img/11.png)

A new window will appear where you can enter the settings for your interactable object. Set the touch highlight color to some green color and make sure that all the checkboxes matches to the image below. The color used for highlight in solution scene was: **#28CA3000**. Click Setup selected objects, close the setup window and look at your cube’s components in the Inspector window.

![image missing](docs/img/12.png)

As you can see, a bunch of scripts have been added to your cube which handle all the basic interactions without you having to write a single line of code. The cube is now ready for interactions in VR and will highlight on touch. Run Game mode and test it out using the simulator. If you have the possibility to test it using VR headset and controllers, the cube can now be grabbed and lifted.

![image missing](docs/img/13.png)
![image missing](docs/img/14.png)

Play around and add some other 3D objects to your scene and set up them as interactable. The image below shows the end result of this assignment.

![image missing](docs/img/15.png)

##Step 5 - Adding custom 3D-models
Adding standard 3D-models to our scene that are interactable has lead us to the very basics of creating a VR application. You can pick up an object, hold it and place it wherever you want. In the future, it is at this time you can start to decide what type of VR experience you want to develop. The step from holding a cube to instead holding a key, or perhaps a sword, is not very big. Basically, the only thing that differs is the 3D-model. In this workshop, instead of using the mesh of a cube, we would like to use a custom model. But creating your own custom 3D-models is not something you can do in Unity. The alternatives is to either learn to use a 3D-modeling program e.g. *Blender*, or import already made custom models.

Start this assignment by once again deleting all created standard 3D objects so that only the floor object is left in the scene. Then from the Project window, try to find the file named table and drag and drop it into the scene window. With the table object selected remember the shortcut **F** on your keyboard for focusing the camera on your object. 

![image missing](docs/img/16.png)

As you can see in the hierarchy of your scene content there is now a new game object called **table**. If you open up the arrow you can see that the table object actually consist of a few other game objects. They have all been grouped to their **parent**. If you change the parents transform the children will be affected. But if you change a **children** it will not affect the other ones. Explore the model and play around with its transform. Try changing the parenting gameobject’s Transform and then see the difference by changing a children.

![image missing](docs/img/17.png)

If you look closer to the components that each of the objects in table has you might have seen a difference when it comes to the Mesh Filter and Render components. The object table has neither of the two mentioned components applied. This is because table acts as a parent to **Empty gameobject**. The purpose of this gameobject is to hold its child objects together. Instead of moving every gameobject a model has, we can move the parent and the children will follow.

Continue with adding another custom made 3D-model. Search for **chair** in the project window and drag and drop it to scene as you did with the table model. This model has no children compared to previous model and is represented by a single mesh. When creating a model in, e.g. Blender, you can choose to either keep each submesh separated or merge them into a single mesh. Duplicate the chair gameobject in the hierarchy and place them both along the table. In the solution scene it looks like the image below.

![image missing](docs/img/18.png)

Add a third game object named **bowlWithApple** to the scene and place it on the table. This time we have a parent called bowlWithApple as the parent, that holds the children gameobjects of a bowl and apples.

![image missing](docs/img/19.png)

To take advantage of our previous knowledge of making a gameobject interactable we will now apply this to all apples. Make sure that all apples are selected in the hierarchy (do NOT select to bowl), then from the top menu go Window->VRTK->Setup Interactable Object. The highlight color is left unchanged with hex color: *#00000000*.

![image missing](docs/img/20.png)

Now when the apples are interactable, enter Game mode to look closer on your created game objects.

Who stole the apples? Where did they go?
Setting up an object for being interactable adds a component named Rigidbody to the gameobject. With this component you can set gravity to the object, which would make it fall if you drop it. It will not stop until it collides with another gameobject. A collision between two objects requires that both objects has a component for handling collisions. To make all the apples ready for collisions, select them all in the hierarchy, go to the Inspector window where all components are listed, click **Add Component**, search for **Sphere Collider** and press enter. This will apply a spherical collider to each apple. Enter Game mode again and see if you can find your apples.

This time the apples will stop falling after hitting the floor. Since the Floor object has a collider the apples wont fall through. But we want them to stay in the bowl at the table. Thus, the bowl also needs a collider. But the bowl is neither round or square, it is hollowed out. So if we cannot use a Box collider or Sphere collider to make the object collidable, there is a component called **Mesh collider**. A mesh collider creates a collider based on the gameobjects mesh, which is exactly what we want in this case. But why not use a mesh collider for every object that needs collision? The benefits from using it is that you can apply it to any shape you want and that you get better precision of collisions. However, it comes with the price of higher processing overhead when calculating collisions, and in a game with many game objects this could be demanding. 

Add the Mesh collider to the **Bowl** object and see how the apples stay when entering Game mode.

##Step 6 - Adding textures to your game objects
With all the custom 3D-models added to the scene it still feels and looks a bit boring. To get the immersive experience that the game objects in front of us are real we need to add some color in our scene. As we did with the floor material, we could change the color of the chairs by editing the color in the **ChairMaterial**. 

![image missing](docs/img/21.png)

But this does not give the realistic feeling of that the chair is made of wood. To solve this we need textures, images on wood, that can be assigned to our chair object’s material. In the Project window, search for ChairMaterialTexture and drag and drop this material on your chair object in the scene. This action will replace the old material ChairMaterial to **ChairMaterialTexture**. 

![image missing](docs/img/22.png)

Here is a comparative image of a chair that uses textures and one that only uses color.

![image missing](docs/img/23.png)

Do this to change to texture material on all your gameobjects:

* On all apples, replace the material AppleMaterial to AppleMaterialTexture
* On the bowl, replace the material WoodBowlMaterial to WoodBowlMaterialTexture
* On TableTop object, replace the material RedWoodtable to tableTopMaterial
* On Floor object, replace the material FloorMaterial to WoodFloorMaterial 

##Final result of this project:


![image missing](docs/img/24.png)
![image missing](docs/img/25.png)
![image missing](docs/img/26.png)